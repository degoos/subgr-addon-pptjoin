package com.degoos.subgr.pptjoin.manager;


import com.degoos.subgr.pptjoin.PPTJoin;
import com.degoos.wetsponge.config.ConfigAccessor;

import java.io.File;
import java.util.List;
import java.util.Map;

public class FileManager implements Manager {
    private ConfigAccessor configuration;


    public void load() {
        File pluginFolder = PPTJoin.getInstance().getDataFolder();

        configuration = new ConfigAccessor(new File(pluginFolder, "config.yml"));
        this.poblateGroups();
    }

    public ConfigAccessor getConfiguration() {
        return configuration;
    }


    public void poblateGroups() {
        PPTJoin.getInstance().getArenaGroups().clear();

        configuration.getSection("groups").getKeys(false).forEach(group -> PPTJoin.getInstance().addArenasToGroup(group, configuration.getStringList("groups." + group), false));
    }


    public void saveGroups(Map<String, List<String>> groups) {
        groups.forEach((group, arenas) -> configuration.set("groups." + group, arenas));
        this.getConfiguration().save();
    }

}
