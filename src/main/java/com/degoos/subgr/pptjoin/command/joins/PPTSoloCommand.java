package com.degoos.subgr.pptjoin.command.joins;

import com.degoos.languages.api.LanguagesAPI;
import com.degoos.subgr.pptjoin.PPTJoin;
import com.degoos.subgr.SUBGR;
import com.degoos.subgr.object.match.Match;
import com.degoos.subgr.object.player.SPlayer;
import com.degoos.subgr.util.CommandUtils;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.entity.living.player.WSPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PPTSoloCommand extends WSSubcommand {

    public PPTSoloCommand(WSRamifiedCommand command) {
        super("solo", command);
    }

    @Override
    public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        if (!CommandUtils.checkPlayer(commandSource) || !CommandUtils.checkPermission(commandSource, "subgr.command.join"))
            return;
        SPlayer player = ((WSPlayer) commandSource).getProperty(SPlayer.PROPERTY_NAME, SPlayer.class).orElse(null);
        if (player == null) return;

        if (player.isPlaying()) {
            LanguagesAPI.sendMessage(commandSource, "command.general.error.alreadyPlaying", PPTJoin.getInstance());
            return;
        }

        if (remainingArguments.length != 0) {
            LanguagesAPI.sendMessage(commandSource, "command.general.error.invalidArguments", PPTJoin.getInstance());
            return;
        }

        Optional<Match> match = PPTJoin.getInstance().getMatchToPlay(1);

        if (!match.isPresent()) {
            LanguagesAPI.sendMessage(commandSource, "command.general.error.arenasNotAvailable", PPTJoin.getInstance());
            return;
        }

        match.get().join(player);
    }

    @Override
    public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        return new ArrayList<>();
    }
}
