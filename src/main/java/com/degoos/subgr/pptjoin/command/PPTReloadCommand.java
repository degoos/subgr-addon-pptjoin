package com.degoos.subgr.pptjoin.command;


import com.degoos.languages.api.LanguagesAPI;
import com.degoos.subgr.pptjoin.PPTJoin;
import com.degoos.subgr.pptjoin.loader.ManagerLoader;
import com.degoos.subgr.pptjoin.manager.FileManager;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class PPTReloadCommand extends WSSubcommand {

    public PPTReloadCommand(WSRamifiedCommand command) {
        super("reload", command);
    }


    @Override
    public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        if (!commandSource.hasPermission("subgr.admin")) {
            LanguagesAPI.sendMessage(commandSource, "command.general.error.noPermission", PPTJoin.getInstance());
        } else {
            Objects.requireNonNull(ManagerLoader.getManager(FileManager.class)).load();
            LanguagesAPI.sendMessage(commandSource, "command.general.success.reloaded", PPTJoin.getInstance());
        }
    }

    @Override
    public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
        return new ArrayList<>();
    }
}
