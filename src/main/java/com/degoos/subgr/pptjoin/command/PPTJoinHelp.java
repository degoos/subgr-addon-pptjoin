package com.degoos.subgr.pptjoin.command;

import com.degoos.languages.api.LanguagesAPI;
import com.degoos.subgr.pptjoin.PPTJoin;
import com.degoos.subgr.SUBGR;
import com.degoos.subgr.object.match.Match;
import com.degoos.subgr.object.player.SPlayer;
import com.degoos.subgr.util.CommandUtils;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.entity.living.player.WSPlayer;
import com.degoos.wetsponge.text.WSText;
import com.degoos.wetsponge.text.action.click.WSSuggestCommandAction;
import com.degoos.wetsponge.text.action.hover.WSShowTextAction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class PPTJoinHelp extends WSSubcommand {

	public PPTJoinHelp(WSRamifiedCommand command) {
		super("help", command);
	}

	@Override
	public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		if(arguments.length == 1) {
			if(PPTJoin.getInstance().getArenaGroups().keySet().stream().anyMatch(key -> key.toLowerCase().equalsIgnoreCase(arguments[0]))) {
				if (!CommandUtils.checkPlayer(commandSource) || !CommandUtils.checkPermission(commandSource, "subgr.command.join"))
					return;
				SPlayer player = ((WSPlayer) commandSource).getProperty(SPlayer.PROPERTY_NAME, SPlayer.class).orElse(null);
				if (player == null) return;

				if (player.isPlaying()) {
					LanguagesAPI.sendMessage(commandSource, "command.general.error.alreadyPlaying", PPTJoin.getInstance());
					return;
				}

				Optional<Match> match = PPTJoin.getInstance().getMatchGroupToPlay(arguments[0]);

				if (!match.isPresent()) {
					LanguagesAPI.sendMessage(commandSource, "command.general.error.arenasNotAvailable", PPTJoin.getInstance());
					return;
				}

				match.get().join(player);
			} else {
				this.sendHelpMessage(commandSource);
			}
		} else {
			this.sendHelpMessage(commandSource);
		}
	}

	private void sendHelpMessage(WSCommandSource commandSource) {
		LanguagesAPI.sendMessage(commandSource, "command.general.help.header", false, PPTJoin.getInstance());
		getCommand().getSubcommands().forEach(subcommand -> commandSource.sendMessage(getCommand(commandSource, subcommand.getName())));
		LanguagesAPI.sendMessage(commandSource, "command.general.help.footer", false, PPTJoin.getInstance());
	}

	@Override
	public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		return new ArrayList<>();
	}

	private WSText getCommand(WSCommandSource source, String command) {
		WSText commandName = LanguagesAPI.getMessage(source, "command.general.help." + command, false, PPTJoin.getInstance()).orElse(WSText.of(command));
		WSText.Builder text = LanguagesAPI.getMessage(source, "command.general.help.command", false, PPTJoin.getInstance(), "<COMMAND>", commandName.toFormattingText())
			.orElse(WSText.getByFormattingText(command)).toBuilder();
		text.hoverAction(WSShowTextAction
			.of(LanguagesAPI.getMessage(source, "command.general.help." + command + "HV", false, PPTJoin.getInstance()).orElse(WSText.empty())));
		text.clickAction(WSSuggestCommandAction.of(commandName.toFormattingText()));
		return text.build();
	}
}
