package com.degoos.subgr.pptjoin.command;

import com.degoos.subgr.pptjoin.command.joins.PPTDuoCommand;
import com.degoos.subgr.pptjoin.command.joins.PPTSoloCommand;
import com.degoos.subgr.pptjoin.command.joins.PPTSquadCommand;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;

public class PPTJoinCommand extends WSRamifiedCommand {

	public PPTJoinCommand() {
		super("spptj", "SUBGR PPTJoin Command.", null, new String[]{"pptjoin", "stj"});

		PPTJoinHelp help = new PPTJoinHelp(this);
		setNotFoundSubcommand(help);
		addSubcommand(help);

		addSubcommand(new PPTAddArenaToGroup(this));
		addSubcommand(new PPTReloadCommand(this));
		addSubcommand(new PPTSoloCommand(this));
		addSubcommand(new PPTDuoCommand(this));
		addSubcommand(new PPTSquadCommand(this));
	}
}
