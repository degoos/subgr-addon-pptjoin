package com.degoos.subgr.pptjoin.command;

import com.degoos.languages.api.LanguagesAPI;
import com.degoos.subgr.pptjoin.PPTJoin;
import com.degoos.subgr.SUBGR;
import com.degoos.subgr.manager.ArenaManager;
import com.degoos.subgr.object.player.SPlayer;
import com.degoos.subgr.util.CommandUtils;
import com.degoos.wetsponge.command.WSCommandSource;
import com.degoos.wetsponge.command.ramified.WSRamifiedCommand;
import com.degoos.wetsponge.command.ramified.WSSubcommand;
import com.degoos.wetsponge.entity.living.player.WSPlayer;

import java.util.ArrayList;
import java.util.List;

public class PPTAddArenaToGroup extends WSSubcommand {

	public PPTAddArenaToGroup(WSRamifiedCommand command) {
		super("addArenaToGroup", command);
	}

	@Override
	public void executeCommand(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		if (!commandSource.hasPermission("subgr.admin")) {
			LanguagesAPI.sendMessage(commandSource, "command.general.error.noPermission", PPTJoin.getInstance());
			return;
		}
		if (!CommandUtils.checkPlayer(commandSource)) return;
		SPlayer player = ((WSPlayer) commandSource).getProperty(SPlayer.PROPERTY_NAME, SPlayer.class).orElse(null);
		if (player == null) return;

		if(remainingArguments.length != 2) {
			LanguagesAPI.sendMessage(commandSource, "command.general.error.invalidArguments", PPTJoin.getInstance());
			return;
		}

		if(!com.degoos.subgr.loader.ManagerLoader.getManager(ArenaManager.class).getArena(remainingArguments[1]).isPresent()) {
            LanguagesAPI.sendMessage(commandSource, "command.general.error.arenaDoesntExist", PPTJoin.getInstance());
            return;
        }

		PPTJoin.getInstance().addArenaToGroup(remainingArguments[0], remainingArguments[1], true);
		LanguagesAPI.sendMessage(player.getPlayer(), "command.general.success.addingArenaToGroup", PPTJoin.getInstance(), "<ARENA>", remainingArguments[1], "<GROUP>", remainingArguments[0]);
	}

	@Override
	public List<String> sendTab(WSCommandSource commandSource, String command, String[] arguments, String[] remainingArguments) {
		return new ArrayList<>();
	}
}
