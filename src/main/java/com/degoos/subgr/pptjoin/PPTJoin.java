package com.degoos.subgr.pptjoin;


import com.degoos.subgr.pptjoin.command.PPTJoinCommand;
import com.degoos.subgr.pptjoin.loader.ManagerLoader;
import com.degoos.subgr.pptjoin.manager.FileManager;
import com.degoos.subgr.manager.MatchManager;
import com.degoos.subgr.object.match.Match;
import com.degoos.wetsponge.WetSponge;
import com.degoos.wetsponge.command.WSCommandManager;
import com.degoos.wetsponge.plugin.WSPlugin;

import java.util.*;

public class PPTJoin extends WSPlugin {
    private static PPTJoin instance;
    private MatchManager matchManager;
    private FileManager fileManager;
    private static Map<String, List<String>> arenaGroups;


    @Override
    public void onEnable() {
        instance = this;
        arenaGroups = new HashMap<>();

        ManagerLoader.load();

        fileManager = ManagerLoader.getManager(FileManager.class);
        matchManager = com.degoos.subgr.loader.ManagerLoader.getManager(MatchManager.class);

        WSCommandManager cm = WetSponge.getCommandManager();
        cm.addCommand(new PPTJoinCommand());
    }


    public static PPTJoin getInstance() {
        return instance;
    }


    public Map<String, List<String>> getArenaGroups() {
        return arenaGroups;
    }


    public void addArenaToGroup(String group, String arena, boolean saveFile) {
        List<String> arenas = new ArrayList<>();
        if(arenaGroups.containsKey(group)) {
            arenas = arenaGroups.get(group);
        }
        arenas.add(arena);
        arenaGroups.put(group, arenas);

        if (saveFile) fileManager.saveGroups(arenaGroups);
    }

    public void addArenasToGroup(String group, List<String> arenas, boolean saveFile) {
        arenaGroups.put(group, arenas);

        if (saveFile) fileManager.saveGroups(arenaGroups);
    }

    public Optional<Match> getMatchToPlay(int playersPerTeam) {
        return matchManager.getMatches().stream().filter(Match::canJoin).filter(match -> match.getArena().getArenaDatabase().getPlayersPerTeam() == playersPerTeam).max(Comparator.comparing(Match::getPlayerAmount));
    }

    public Optional<Match> getMatchGroupToPlay(String groupName) {
        List<String> maps = new ArrayList<>(arenaGroups.get(groupName));
        return matchManager.getMatches().stream().filter(Match::canJoin).filter(match -> maps.contains(match.getArena().getName())).max(Comparator.comparing(Match::getPlayerAmount));
    }
}
